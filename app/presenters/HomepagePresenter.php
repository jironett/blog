<?php

namespace App\Presenters;

use Nette;
use App\Model;


class HomepagePresenter extends BasePresenter
{
    /** @var \App\Model\ArticleManager @inject */
    public $articleManager;

	public function renderDefault()
	{
		$this->template->articles = $this->articleManager->getAll();
	}

}
