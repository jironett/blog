<?php

namespace App\Presenters;


use App\Forms\AddArticleForm;

class BackendArticlePresenter extends BaseBackendPresenter
{
    /** @var  AddArticleForm @inject */
    public $addArticleFormFactory;

    protected function createComponentAddArticleForm()
    {
        return $this->addArticleFormFactory->create(function () {
            $this->em->flush();
        });
    }

}