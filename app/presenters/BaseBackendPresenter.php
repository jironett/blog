<?php

namespace App\Presenters;

use Nette;
use App\Model;


/**
 * Base presenter for all application presenters.
 */
abstract class BaseBackendPresenter extends Nette\Application\UI\Presenter
{
    /** @var \Kdyby\Doctrine\EntityManager @inject */
    public $em;

    public function startup()
    {
        parent::startup();
        if(!$this->user->isLoggedIn()){
            $this->redirect("Sign:in");
        }
    }
}
