<?php

namespace App\Model;

use Doctrine\ORM\EntityRepository;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\Security\Passwords;


/**
 * Users management.
 */
class UserManager implements Nette\Security\IAuthenticator
{
	use Nette\SmartObject;


	/** @var EntityManager */
	private $em;

    /** @var  EntityRepository */
    private $daoUser;


	public function __construct(EntityManager $entityManager)
	{
	    $this->em = $entityManager;
        $this->daoUser = $this->em->getRepository('App\Model\Entities\User');
	}


	/**
	 * Performs an authentication.
	 * @return Nette\Security\Identity
	 * @throws Nette\Security\AuthenticationException
	 */
	public function authenticate(array $credentials)
	{
		list($username, $password) = $credentials;
		$user = $this->daoUser->findOneBy(["username" => $username]);

		if (!$user) {
			throw new Nette\Security\AuthenticationException('Username', self::IDENTITY_NOT_FOUND);

		} elseif (!Passwords::verify($password, $user->getPassword())) {
			throw new Nette\Security\AuthenticationException('Password', self::INVALID_CREDENTIAL);
		}

		$arr = ["id" => $user->getId(),
                "username" => $user->getUsername(),
                "name" => $user->getName(),
                "surname" => $user->getSurname()];
		return new Nette\Security\Identity($user->getId(), 'author', $arr);
	}

	/**
	 * Adds new user.
	 * @param  string
	 * @param  string
	 * @param  string
	 * @return void
	 * @throws DuplicateNameException
	 */
	public function add($username, $email, $password)
	{
		try {
			$this->database->table(self::TABLE_NAME)->insert([
				self::COLUMN_NAME => $username,
				self::COLUMN_PASSWORD_HASH => Passwords::hash($password),
				self::COLUMN_EMAIL => $email,
			]);
		} catch (Nette\Database\UniqueConstraintViolationException $e) {
			throw new DuplicateNameException;
		}
	}

}



class DuplicateNameException extends \Exception
{}
