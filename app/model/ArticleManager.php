<?php

namespace App\Model;

use Kdyby\Doctrine\EntityManager;
use Doctrine\ORM\EntityRepository;


class ArticleManager
{
    /** @var EntityManager */
    private $em;

    /** @var  EntityRepository */
    private $daoArticle;


    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
        $this->daoArticle = $this->em->getRepository('App\Model\Entities\Article');
    }

    public function getAll()
    {
        return $this->daoArticle->findAll();
    }
}