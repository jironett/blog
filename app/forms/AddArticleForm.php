<?php
namespace App\Forms;

use App\Model\Entities\Article;
use Kdyby\Doctrine\EntityManager;
use Nette\Security\User;
use Nette\Application\UI\Form;

class AddArticleForm
{

    /** @var FormFactory */
    private $factory;

    /** @var User */
    private $user;

    /** @var EntityManager */
    private $em;

    /** @var \Kdyby\Doctrine\EntityRepository  */
    private $daoUser;

    public function __construct(FormFactory $factory, User $user, EntityManager $em)
    {
        $this->factory = $factory;
        $this->user = $user;
        $this->em = $em;
        $this->daoUser = $em->getRepository('App\Model\Entities\User');
    }


    /**
     * @return Form
     */
    public function create(callable $onSuccess)
    {
        $form = $this->factory->create();
        $form->addText('title', 'Název')
            ->setRequired('Prosím vyplňte název článku');

        $form->addTextArea('content', 'Text:')
            ->setRequired('Prosím vyplňte název článku.');

        $form->addSubmit('submit', 'Uložit');

        $form->onValidate[] = [$this, 'validate'];
        $form->onSuccess[] = function (Form $form, $values) use ($onSuccess) {
            $user = $this->daoUser->find($this->user->id);
            $article = new Article();
            $article->setAuthor($user);
            $article->setTitle($values->title);
            $article->setContent($values->content);
            $this->em->persist($article);
            $onSuccess();
        };
        return $form;
    }

    public function validate(Form $form, $values)
    {
        if($values->title === "" || $values->content === "")
        {
            $form->addError("Nebylo vyplněno některé povinné pole");
        }
    }
}
